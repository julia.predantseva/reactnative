import * as React from 'react';
import { View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux';

import {
  HomeScreen,
  SearchItemScreen,
} from './screens';
import store from './redux/store';
import styles from './styles';


const Stack = createStackNavigator();

// eslint-disable-next-line no-console
console.disableYellowBox = true;

export default class App extends React.Component {
  containerRef = React.createRef();

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <NavigationContainer ref={this.containerRef}>
            <Stack.Navigator headerMode="none">
              <Stack.Screen name="Root" component={HomeScreen} />
              <Stack.Screen name="SearchItem" component={SearchItemScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </Provider>
    );
  }
}
