export const LOAD_LIST_SUCCESS = 'load.list.success';
export const LOAD_LIST_REQUEST = 'load.list.request';
export const LOAD_LIST_FAILURE = 'load.list.failure';
export const SEARCH_REQUEST = 'google.api.search';
export const SEARCH_CLEAR = 'search.claer';

export const loadListRequest = () => ({ type: LOAD_LIST_REQUEST });
export const loadListSuccess = (payload) => ({ type: LOAD_LIST_SUCCESS, payload });
export const loadListFailure = (payload) => ({ type: LOAD_LIST_FAILURE, payload });
export const searchRequest = (payload) => ({ type: SEARCH_REQUEST, payload });
export const searchClear = () => ({ type: SEARCH_CLEAR });
