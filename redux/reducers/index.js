import { combineReducers } from 'redux';
import search from './googleSearch';

export default combineReducers({ search });
