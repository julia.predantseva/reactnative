import { createReducer } from '@reduxjs/toolkit';
import {
  LOAD_LIST_REQUEST,
  LOAD_LIST_SUCCESS,
  LOAD_LIST_FAILURE,
  SEARCH_CLEAR,
} from '../actions/googleSearch';

const initState = {
  data: [],
  err: null,
  loading: false,
};

export default createReducer(initState, {
  [LOAD_LIST_SUCCESS]: (state, { payload }) => { state.data = payload; state.loading = false; },
  [LOAD_LIST_REQUEST]: (state) => { state.loading = true; state.err = null; },
  [LOAD_LIST_FAILURE]: (state, { payload }) => { state.err = payload; state.loading = false; },
  [SEARCH_CLEAR]: (state) => { state.data = []; },
});
