import { takeEvery, call, put } from 'redux-saga/effects';

import api from '../../api';
import {
  loadListSuccess,
  loadListFailure,
  loadListRequest,
  SEARCH_REQUEST,
} from '../actions/googleSearch';

function* searchRequestSaga(action) {
  yield put(loadListRequest());
  try {
    const { items } = yield call(api.searchRequest, action.payload);

    yield put(loadListSuccess(items || ''));
  } catch (err) {
    yield put(loadListFailure(err));
  }
}

export default function* () {
  yield takeEvery(SEARCH_REQUEST, searchRequestSaga);
}
