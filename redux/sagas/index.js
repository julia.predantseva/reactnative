import { fork, all } from 'redux-saga/effects';

import searchRequestSaga from './searchRequestSaga';

export default function* () {
  yield all([
    fork(searchRequestSaga),
  ]);
}
