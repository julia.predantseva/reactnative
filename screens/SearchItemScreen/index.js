import React from 'react';
import {
  Text,
  ScrollView,
  Linking,
  Image,
  View,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {
  shape,
  string,
  func,
  number,
} from 'prop-types';

import styles from './styles';

const screenWidth = Dimensions.get('window').width;

function SearchItemScreen({ navigation, route: { params } }) {
  return (
    <View style={styles.wrapper}>
      <Ionicons
        name="ios-arrow-back"
        style={styles.backIcon}
        onPress={navigation.goBack}
      />
      <ScrollView
        minimumZoomScale={1}
        maximumZoomScale={5}
        contentContainerStyle={styles.imgWrapper}
      >
        <Image
          style={{
            width: '100%',
            height: params.image.height * (screenWidth / params.image.width),
          }}
          source={{ uri: params.link }}
        />

      </ScrollView>
      <View style={styles.footer}>
        <Text style={styles.text}>
          {params.title}
        </Text>
        <TouchableWithoutFeedback
          onPress={() => Linking.openURL(params.image.contextLink)}
        >
          <Text style={styles.btn}>
            {params.displayLink}
          </Text>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
}

SearchItemScreen.propTypes = {
  route: shape({
    params: shape({
      title: string,
      displayLink: string,
      image: shape({
        heigth: number,
        width: number,
        contextLink: string,
      }),
    }),
  }).isRequired,
  navigation: shape({
    navigate: func.isRequired,
  }).isRequired,
};

export default SearchItemScreen;
