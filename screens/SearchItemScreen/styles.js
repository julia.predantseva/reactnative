import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  imgWrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  footer: {
    padding: 20,
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: '100%',
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },
  text: {
    fontSize: 18,
  },
  btn: {
    backgroundColor: 'black',
    color: 'white',
    padding: 10,
    marginTop: 30,
    marginLeft: 'auto',
  },
  backIcon: {
    color: '#075fff',
    fontSize: 40,
    position: 'absolute',
    top: 40,
    left: 15,
    zIndex: 22,
  },
});
