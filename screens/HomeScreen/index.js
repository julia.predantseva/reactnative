/* eslint no-shadow: ['error', { 'allow': ['searchRequest', 'searchClear'] }] */
import * as React from 'react';
import { connect } from 'react-redux';
import {
  SafeAreaView,
  TextInput,
  Text,
  ScrollView,
  View,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {
  shape,
  arrayOf,
  string,
  func,
} from 'prop-types';

import Item from './ListItem';
import { searchRequest, searchClear } from '../../redux/actions/googleSearch';
import styles from './styles';


class HomeScreen extends React.Component {
  inputRef = React.createRef();

  getSearchValue = () => {
    const { searchRequest } = this.props;
    searchRequest(this.inputRef.current._lastNativeText);
  }

  clearSeach = () => {
    const { searchClear } = this.props;

    this.inputRef.current.clear();
    searchClear();
  }

  render() {
    const { search, navigation } = this.props;

    return (
      <ScrollView style={styles.container}>
        <Text style={styles.title}>Image Search</Text>
        <View>
          <TextInput
            ref={this.inputRef}
            style={styles.input}
            placeholder="Search"
            onSubmitEditing={this.getSearchValue}
          />
          <Ionicons
            name="ios-close-circle"
            style={styles.closeIcon}
            onPress={this.clearSeach}
          />
        </View>
        {search.data && (
          <SafeAreaView style={styles.container}>
            <ScrollView contentContainerStyle={styles.imgGalleryContainer}>
              {search.data.map((item) => (
                <Item data={item} navigation={navigation} key={item.link} />
              ))}
            </ScrollView>
          </SafeAreaView>
        )}
        {search.err && <Text style={styles.err}> Something went wrong... </Text> }
      </ScrollView>
    );
  }
}

HomeScreen.propTypes = {
  search: shape({
    data: arrayOf(shape({
      title: string,
      link: string,
    })),
    err: string,
  }).isRequired,
  searchRequest: func.isRequired,
  navigation: shape({
    navigate: func.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ search }) => ({ search });

const mapDispatchToProps = {
  searchRequest,
  searchClear,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);
