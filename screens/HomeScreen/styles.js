import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffebe1',
    paddingTop: 50,
  },
  input: {
    height: 40,
    borderWidth: 1,
    backgroundColor: '#fff7e6',
    paddingLeft: 15,
    fontSize: 18,
    position: 'relative',
  },
  imgGalleryContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingBottom: 50,

  },
  title: {
    fontSize: 34,
    textAlign: 'center',
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
  },
  closeIcon: {
    color: 'grey',
    fontSize: 25,
    position: 'absolute',
    right: 15,
    top: '20%',
  },
  err: {
    color: 'red',
    textAlign: 'center',
    fontSize: 18,
  },
});
