import { StyleSheet, Dimensions } from 'react-native';

const screenWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  wrapper: {
    width: screenWidth / 2,
  },
  content: {
    width: '80%',
    alignContent: 'center',
    paddingTop: 20,
    marginTop: 0,
    marginRight: 'auto',
    marginBottom: 0,
    marginLeft: 'auto',
    alignItems: 'center',
  },
  title: {
    position: 'absolute',
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    bottom: 0,
    width: '100%',
    fontSize: 18,
    padding: 7,
    borderColor: '#000',
    borderWidth: 1,
  },
  img: {
    width: '100%',
    position: 'relative',
    height: screenWidth / 2.5,
  },
});
