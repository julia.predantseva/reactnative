import * as React from 'react';
import {
  View, TouchableOpacity, Text, Image,
} from 'react-native';
import { shape, string, func } from 'prop-types';

import styles from './styles';

function Item({ navigation, data }) {
  return (
    <TouchableOpacity
      style={styles.wrapper}
      onPress={() => navigation.navigate('SearchItem', { ...data })}
    >
      <View style={styles.content}>
        <Image
          style={styles.img}
          source={{ uri: data.link }}
        />
        <Text style={styles.title} numberOfLines={1}>{data.title}</Text>
      </View>
    </TouchableOpacity>
  );
}

Item.propTypes = {
  data: shape({
    title: string.isRequired,
    link: string.isRequired,
  }).isRequired,
  navigation: shape({
    navigate: func.isRequired,
  }).isRequired,
};

export default Item;
