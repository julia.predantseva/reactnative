import data from './mockData';

class Api {
  loadList(query) {
    return fetch(`https://www.googleapis.com/customsearch/v1?key=AIzaSyBO1cbOiVCHrtiWXHwHOUwQJmB-4XWRk-0&cx=003391602424681937004:rzcxcouxlqz&q=${query}&searchType=image`).then((res) => res.json());
  }

  searchRequest(query) {
    return Promise.resolve(data)
      .then((res) => res.find((el) => el.queries.request[0].searchTerms === query.toLowerCase()));
  }
}

export default new Api();
